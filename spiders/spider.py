# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from scrapy.selector import Selector
import re
from sebaidusearch.items import SebaidusearchItem
from time import sleep


class SpiderSpider(scrapy.Spider):
    name = 'spider'
    allowed_domains = ['baidu.com']
#    def parse(self, response):
#        pass
    start_date='2018-06-09'
    end_date='2018-06-25'
    url='https://www.baidu.com/s?wd=%s' % 'scrapy'
    page=5
#    search_list=['世界杯','scrapy']
#    crawled_urllist=[]


    def start_requests(self):
#        while self.search_list.__len__ ():
#            word=self.search_list.pop()
#            self.crawled_urllist.append(word)
        yield Request(url=self.url,callback=self.parse_content)
    
    def parse_content(self,response):
        url=self.url
        driver=webdriver.Chrome()
        driver.get(url)
        sleep(1)
        if response.xpath('//*[@id="container"]/div[2]/div/div[2]/div/i'):
            module=driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[2]/div/i')
            module.click()
#                locator=(By.LINK_TEXT,('收起工具')) 
            sleep(1)
            locator=driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[1]/i')
            ActionChains(driver).click(locator)
#                print(driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[2]').text)
            if driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[3]').text =='时间不限':
                locator2=driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[3]')
                locator2.click()
#                ActionChains(driver).click(locator2)
#                locator1=driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[3]/i')
#                WebDriverWait(driver,20,0.5).until(EC.)
#                locator1.click()
#                locator2=(By.LINK_TEXT('自定义'))
#                WebDriverWait(driver,20,0.5).until(EC.presence_of_element_located(locator2))
                locator3=driver.find_element_by_xpath('//*[@id="c-tips-container"]/div[2]/div/div/ul/li[6]/a')
                input1=driver.find_element_by_name('st') #start date
                input2=driver.find_element_by_name('et') #end date
                input1.clear()
                ActionChains(driver).move_to_element(input1).send_keys(self.start_date).perform()
                input2.clear()
                ActionChains(driver).move_to_element(input2).send_keys(self.end_date).perform()
                locator3.click()
                sleep(1)
                page=0
                while page<self.page: #set target page
                    sleep(1)
                    source=driver.page_source
                    pattern=re.compile(r'(.*?)\s?-') 
                    res=Selector(text=source)
                    title_list=res.xpath('//div[@class="result c-container "]')
                    for index in title_list:
                        item=SebaidusearchItem()
                        title_index=title_list.index(index)
#                        print(title_index)
                        if res.xpath('//div[@class="result c-container "]['+str(title_index+1)+']/div/span[@class=" newTimeFactor_before_abs m"]'):
                            times=res.xpath('//div[@class="result c-container "]['+str(title_index+1)+']/div/span[@class=" newTimeFactor_before_abs m"]/text()').extract()
                            titles=res.xpath('//div[@class="result c-container "]['+str(title_index+1)+']/h3/a')
                            title=titles.xpath('string(.)').extract()
                            for i in times:
                                a=pattern.findall(i)
                                item['time']=a
                                item['title']=title
                            yield item
                    next_page=driver.find_element_by_xpath("//a[contains(text(),'下一页')]")
                    sleep(1)
                    if next_page:
                        next_page.click()
                        WebDriverWait(driver, 10, 0.5).until(EC.text_to_be_present_in_element((By.XPATH,"//a[contains(text(),'下一页')]"),'下一页'))
                    page=page+1
                driver.close()

            elif driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[2]').text=='时间不限':
                locator2=driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[2]')
                locator2.click()
                locator3=driver.find_element_by_xpath('//*[@id="c-tips-container"]/div[1]/div/div/ul/li[6]/a')
                input1=driver.find_element_by_name('st') #start date 
                input1.clear()
                input2=driver.find_element_by_name('et') #end date
                ActionChains(driver).move_to_element(input1).send_keys(self.start_date).perform()
                input2.clear()
                ActionChains(driver).move_to_element(input2).send_keys(self.end_date).perform()
                locator3.click()
                sleep(1)
                page=0
                while page<self.page: #set target page
                    sleep(1)
                    source=driver.page_source
#               print(source)
                    pattern=re.compile(r'(.*?)\s?-') 
                    res=Selector(text=source)
                    title_list=res.xpath('//div[@class="result c-container "]')
                    for index in title_list:
                        item=SebaidusearchItem()
                        title_index=title_list.index(index)
                        if res.xpath('//div[@class="result c-container "]['+str(title_index+1)+']/div/span[@class=" newTimeFactor_before_abs m"]'):
                            times=res.xpath('//div[@class="result c-container "]['+str(title_index+1)+']/div/span[@class=" newTimeFactor_before_abs m"]/text()').extract()
                            titles=res.xpath('//div[@class="result c-container "]['+str(title_index+1)+']/h3/a')
                            title=titles.xpath('string(.)').extract()
                            for i in times:
                                a=pattern.findall(i)
                                item['time']=a
                                item['title']=title
                            yield item
                    next_page=driver.find_element_by_xpath("//a[contains(text(),'下一页')]")
                    sleep(1)
                    if next_page:
                        next_page.click()
                        WebDriverWait(driver, 10, 0.5).until(EC.text_to_be_present_in_element((By.XPATH,"//a[contains(text(),'下一页')]"),'下一页'))
                    page=page+1
                driver.close()
                    
                
            
            

            
            
            
            
